﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            listView1.View = View.Details;
            listView1.GridLines = true;

            listView1.Columns.Add("ACC", 50);
            listView1.Columns.Add("BACC", 50);
            listView1.Columns.Add("MEACC", 50);
            listView1.Columns.Add("MAG ACC", 50);
            listView1.Columns.Add("EACC", 50);
            listView1.Columns.Add("AGI", 50);
            listView1.Columns.Add("UDR", 50);
            listView1.Columns.Add("B DR", 50);
            listView1.Columns.Add("ME DR", 50);
            listView1.Columns.Add("MAG DR", 50);
            listView1.Columns.Add("PER", 50);
            listView1.Columns.Add("SNK", 50);
            listView1.Columns.Add("STR", 50);
            listView1.Columns.Add("MAE", 50);
            listView1.Columns.Add("EXD", 50);
            listView1.Columns.Add("BS", 50);
            listView1.Columns.Add("HAX", 50);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
